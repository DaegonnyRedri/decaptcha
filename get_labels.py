# -*- coding: utf-8 -*-
"""
Created on Mon May 21 18:45:55 2018

@author: igor.marques
"""

import os
import pandas as pd



os.chdir("/home/daegonny/code/python/ia/decaptcha/")

#leitura do arquivo de treino
df = pd.read_csv('train.csv', sep="," ,error_bad_lines=False, dtype=str)

list_splited = []

idx = 0

for index, row in df.iterrows():
    print(row['text'])
    list_splited.append({'idx':idx, 'key': 0, 'value': row['text'][0]})
    list_splited.append({'idx':idx, 'key': 1, 'value': row['text'][1]})
    list_splited.append({'idx':idx, 'key': 2, 'value': row['text'][2]})
    list_splited.append({'idx':idx, 'key': 3, 'value': row['text'][3]})


final = pd.DataFrame(list_splited)
df.to_csv('label.csv', sep = ';')