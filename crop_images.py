import sys

import numpy as np
import cv2

rectangleList = []
i=0

im = cv2.imread('/home/joao-psp/Área de Trabalho/img2.png')
im3 = im.copy()

gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
#blur = cv2.GaussianBlur(gray,(3,3),0)
ret,thresh = cv2.threshold(gray,165,255,cv2.THRESH_BINARY_INV)

cv2.imshow("g", gray)
#cv2.imshow("b", blur)
cv2.imshow("t", thresh)

(_, contours,_) = cv2.findContours(thresh.copy(), cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

for contour in contours:

    x,y,w,h = cv2.boundingRect(contour)

    if h>45 and w>50:
        continue
    # discard areas that are too small

    if h<15 or w<15:
        continue

    rectangleList.append((x,y,w,h))
    x = cv2.rectangle(im,(x,y),(x+w,y+h),(255,0,255),1)
    # cv2.imwrite(str(i)+".png",x)
    i= i +1

cv2.drawContours(im3,contours,-1,(0,0,255), 0)
cv2.imshow("contornos", im3)
cv2.imshow("retangulos", im)
cv2.imwrite("1.png",im)
print(i)
cv2.waitKey(5000)
