# -*- coding: utf-8 -*-
"""
Created on Thu May 17 14:39:01 2018

@author: igor.marques
"""

import os
from PIL import Image
#from PIL import ImageOps
#import numpy as np
#import pandas as pd



#os.chdir("/home/daegonny/code/python/ia/decaptcha/")
os.chdir("/home/joao-psp/Documentos/TP_Final_IA/decaptcha/")

count = 400
data = []

threshold = 210


while count <= 600:
    im = Image.open("img/captcha"+str(count)+".png")#.convert('L')
    im = im.crop((26, 2, 130, 44))
    #im = im.point(lambda p: p > threshold and 255)
    #im = ImageOps.invert(im)
    s1 = im.crop((0,0,26,44))
    s1 = s1.resize((32, 32))
    s2 = im.crop((26,0,52,44))
    s2 = s2.resize((32, 32))
    s3 = im.crop((52,0,78,44))
    s3 = s3.resize((32, 32))
    s4 = im.crop((78,0,104,44))
    s4 = s4.resize((32, 32))
    s1.save("cropped/"+str(count)+"_1.png","png")
    #data.append(np.array(s1.getdata()))80
    s2.save("cropped/"+str(count)+"_2.png","png")
    #data.append(np.array(s2.getdata()))
    s3.save("cropped/"+str(count)+"_3.png","png")
    #data.append(np.array(s3.getdata()))
    s4.save("cropped/"+str(count)+"_4.png","png")
    #data.append(np.array(s4.getdata()))

    count = count + 1
#df = pd.DataFrame(data)

#df.to_csv('data.csv', sep = ';')
