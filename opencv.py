#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 19:30:58 2018

@author: daegonny
"""

import os

import numpy as np
import cv2

os.chdir("/home/daegonny/code/python/ia/decaptcha/")

rectangleList = []
i=0

im = cv2.imread('img/captcha1013.png')
im3 = im.copy()

gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
#blur = cv2.GaussianBlur(gray,(3,3),0)
ret,thresh = cv2.threshold(gray,230,255,cv2.THRESH_BINARY_INV)

cv2.imshow("g", gray)
cv2.waitKey(5000)
#cv2.imshow("b", blur)
cv2.imshow("t", thresh)
cv2.waitKey(5000)
"""
(_, contours,_) = cv2.findContours(thresh.copy(), cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

for contour in contours:

    x,y,w,h = cv2.boundingRect(contour)

    # if h>45 and w>50:
    #     continue
    # # discard areas that are too small
    #
    # if h<15 or w<15:
    #     continue

    rectangleList.append((x,y,w,h))
    cv2.rectangle(im,(x,y),(x+w,y+h),(255,0,255),1)
    i= i +1

cv2.drawContours(im3,contours,-1,(0,0,255), 0)
cv2.imshow("contornos", im3)
cv2.imshow("retangulos", im)
print(i)
cv2.waitKey(5000)
"""
